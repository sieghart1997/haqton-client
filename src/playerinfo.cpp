#include "playerinfo.h"

#include <QtQml>

PlayerInfo::PlayerInfo(QObject *parent) : QObject(parent)
{
    qmlRegisterInterface<PlayerInfo>("PlayerInfo", 1);
}

QString PlayerInfo::nickname()
{
    return _nickname;
}

void PlayerInfo::setNickname(const QString &nickname)
{
    if (nickname == _nickname)
        return;

    _nickname = nickname;
    emit nicknameChanged();
}

QString PlayerInfo::email()
{
    return _email;
}

void PlayerInfo::setEmail(const QString &email)
{
    auto aux_email = email.toLower();
    if (aux_email == _email)
        return;

    _email = aux_email;
    emit emailChanged();
}
