/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "core.h"

#include "messagingcontroller.h"
#include "playerinfo.h"
#include "matchcontroller.h"

Q_LOGGING_CATEGORY(core, "solutions.qmob.haqton.core")

Core *Core::_instance = nullptr;

Core::Core(QObject *parent)
    : QObject(parent),
      _messagingController(new MessagingController(this)),
      _playerInfo(new PlayerInfo{this}),
      _matchController(new MatchController{this})
{
    connect(
        _messagingController, &MessagingController::newMessage,
        _matchController, &MatchController::processMessage
    );
    connect(
        _matchController, &MatchController::setTopic,
        _messagingController, &MessagingController::setTopic
    );
}

Core::~Core()
{
    delete _messagingController;
    delete _playerInfo;
    delete _matchController;
}

Core *Core::instance()
{
    if (!_instance) {
        _instance = new Core;
    }
    return _instance;
}

UIController *Core::uiController() const
{
    return _uiController;
}

PlayerInfo *Core::playerInfo() const
{
    return _playerInfo;
}

MatchController *Core::matchController() const
{
    return _matchController;
}
