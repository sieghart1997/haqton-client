#ifndef HAQTONAPI_H
#define HAQTONAPI_H

#include <QNetworkReply>

#define ID const unsigned int
#define BODY const QVariantMap

class HaQtonAPI
{
public:
    HaQtonAPI() = delete;

#define HAQTON_API [[nodiscard]] static QNetworkReply *

    // http://bit.do/api-haqton
    HAQTON_API MatchesWaitingForPlayers();
    HAQTON_API CreateMatch(BODY body);
    HAQTON_API UpdateMatch(ID matchId, BODY body);
    HAQTON_API DeleteMatch(ID matchId);
    HAQTON_API AddPlayerToMatch(ID matchId, BODY body);
    HAQTON_API UpdatePlayer(ID matchId, ID playerId, BODY body);
    HAQTON_API RandomQuestion(ID matchId);
    HAQTON_API ProcessPlayerAnswer(ID matchId, ID playerId, BODY body);

#undef HAQTON_API

private:
    static const QByteArray _AUTHORIZATION;
    static const QUrl _BASE_URL;

    static QNetworkRequest _request(const QString &relativeUrl);

    static QNetworkAccessManager _manager;
};

#endif // HAQTONAPI_H
