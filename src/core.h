/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef CORE_H_
#define CORE_H_

#include <QObject>

class MessagingController;
class PlayerInfo;
class UIController;
class MatchController;

class Core : public QObject
{
    Q_OBJECT

    Q_PROPERTY(PlayerInfo * playerInfo READ playerInfo CONSTANT)
    Q_PROPERTY(UIController * ui READ uiController CONSTANT)
    Q_PROPERTY(MatchController * match READ matchController CONSTANT)

public:
    ~Core() Q_DECL_OVERRIDE;

    static Core *instance();

    PlayerInfo *playerInfo() const;
    UIController *uiController() const;
    MatchController *matchController() const;

private:
    explicit Core(QObject *parent = nullptr);

    static Core *_instance;
    MessagingController *_messagingController;
    PlayerInfo *_playerInfo;
    UIController *_uiController;
    MatchController *_matchController;

};

#endif	// CORE_H_
