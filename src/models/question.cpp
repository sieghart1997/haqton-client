#include "question.h"

#include "../data.h"
#include <QVariant>
#include <QtQml>

Question::Question(const QuestionData &data)
    : QObject(nullptr), _id(data.id), _description(data.description), _rightOption(data.rightOption)
{
    for (const auto &x : data.questionOptions)
    {
        _questionOptions.append(x.description);
    }

    qmlRegisterInterface<Question>("Question", 1);
}

int Question::id() const
{
    return _id;
}

QString Question::description() const
{
    return _description;
}

int Question::rightOption() const
{
    return _rightOption;
}

QList<QString> Question::questionOptions() const
{
    return QList<QString>(_questionOptions.begin(), _questionOptions.end());
}

QVariantMap Question::playerAnswers() const
{
    QVariantMap data;
    for (const auto &x : _playerAnswers)
    {
        data[QString::number(x.first)] = x.second;
    }
    return data;
}

void Question::setPlayerAnswer(const int playerId, const int answer)
{
    _playerAnswers[playerId] = answer;
    Q_EMIT playerAnswersChanged(playerAnswers());
}
