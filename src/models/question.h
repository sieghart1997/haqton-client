#ifndef QUESTION_H
#define QUESTION_H

#include <QObject>
#include <map>

class QuestionData;

class Question : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id READ id CONSTANT)
    Q_PROPERTY(QString description READ description CONSTANT)
    Q_PROPERTY(int rightOption READ rightOption CONSTANT)
    Q_PROPERTY(QList<QString> questionOptions READ questionOptions CONSTANT)
    Q_PROPERTY(QVariantMap playerAnswers READ playerAnswers NOTIFY playerAnswersChanged)

public:
    explicit Question(const QuestionData &data);

    int id() const;
    QString description() const;
    int rightOption() const;
    QList<QString> questionOptions() const;
    QVariantMap playerAnswers() const;

    void setPlayerAnswer(const int playerId, const int answer);

Q_SIGNALS:
    void playerAnswersChanged(QVariantMap);

private:
    int _id;
    QString _description;
    int _rightOption;
    QList<QString> _questionOptions;
    std::map<int, int> _playerAnswers;
};

#endif // QUESTION_H
