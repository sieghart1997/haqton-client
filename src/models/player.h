#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>

class PlayerData;

class Player : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id READ id CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString email READ email CONSTANT)
    Q_PROPERTY(bool approved READ approved CONSTANT)
    Q_PROPERTY(int score READ score NOTIFY scoreChanged)

public:
    explicit Player(const PlayerData&);

    int id() const;
    QString name() const;
    QString email() const;
    bool approved() const;
    int score() const;

    void setScore(int);

Q_SIGNALS:
    void scoreChanged(int);

private:
    const int _id;
    const QString _name;
    const QString _email;
    bool _approved;
    int _score;
};

#endif // PLAYER_H
