#ifndef CURRENTMATCH_H
#define CURRENTMATCH_H

#include <QObject>
#include <QPointer>

class Player;
class Question;

class AvailableMatchData;
class PlayerData;
class QuestionData;
class PlayerAnswerData;

class CurrentMatch : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int matchId READ matchId CONSTANT)
    Q_PROPERTY(int playerId READ playerId CONSTANT)
    Q_PROPERTY(bool isOwner READ isOwner CONSTANT)
    Q_PROPERTY(QList<Player *> players READ players NOTIFY playersChanged)
    Q_PROPERTY(Player *me READ me CONSTANT)
    Q_PROPERTY(Question *question READ question NOTIFY questionChanged)
    Q_PROPERTY(int questionNumber READ questionNumber CONSTANT)
    Q_PROPERTY(QList<Player *> sortedPlayers READ sortedPlayers CONSTANT)

public:
    explicit CurrentMatch(const AvailableMatchData &data);

    int matchId() const;
    int playerId() const;
    bool isOwner() const;
    QList<Player *> players() const;
    Player *me() const;
    Question *question() const;
    int questionNumber() const;
    QList<Player *> sortedPlayers() const;

    void updatePlayers(const QList<PlayerData>&);
    void updateQuestion(const QuestionData&);
    void setPlayerAnswer(const PlayerAnswerData&);

Q_SIGNALS:
    void playersChanged(const QList<Player *>&);
    void questionChanged(const Question&);

private:
    const int _matchId;
    int _playerId;
    bool _isOwner;
    QList<QPointer<Player>> _players;
    QPointer<Question> _question;
    int _questionNumber;
};

#endif // CURRENTMATCH_H
