#include "currentmatch.h"

#include "player.h"
#include "question.h"
#include "../data.h"
#include <QtQml>

CurrentMatch::CurrentMatch(const AvailableMatchData &data)
    : QObject(nullptr), _matchId(data.id), _playerId(0), _question(nullptr), _questionNumber(0)
{
    if (!data.matchPlayers.isEmpty())
    {
        for (const auto &x : data.matchPlayers)
        {
            _players.append(new Player {x});
        }

        _playerId = data.matchPlayers.back().id;

        _isOwner = data.matchPlayers.front().id == data.matchPlayers.back().id;
    }

    qmlRegisterInterface<CurrentMatch>("CurrentMatch", 1);
    qRegisterMetaType<QList<Player *>>("QList<Player *>");
    qRegisterMetaType<Player*>("Player*");
    qRegisterMetaType<Question*>("Question*");

}

int CurrentMatch::matchId() const
{
    return _matchId;
}

int CurrentMatch::playerId() const
{
    return _playerId;
}

bool CurrentMatch::isOwner() const
{
    return _isOwner;
}

QList<Player *> CurrentMatch::players() const
{
    return {_players.begin(), _players.end()};
}

Player *CurrentMatch::me() const
{
    auto it = std::find_if(_players.begin(), _players.end(), [&](Player *x) {
        return x->id() == _playerId;
    });
    if (it != _players.end())
    {
        return *it;
    }
    return nullptr;
}

Question *CurrentMatch::question() const
{
    return _question;
}

int CurrentMatch::questionNumber() const
{
    return _questionNumber;
}

QList<Player *> CurrentMatch::sortedPlayers() const
{
    QList<Player *> list  = players();
    std::sort(list.begin(), list.end(), [](auto a, auto b) {
        return a->score() > b->score();
    });
    return list;
}

void CurrentMatch::updatePlayers(const QList<PlayerData> &data)
{
    _players.clear();
    for (const auto &x : data)
    {
        _players.append(new Player {x});
    }
    _isOwner = data.front().id == _playerId;

    Q_EMIT playersChanged(players());
}

void CurrentMatch::updateQuestion(const QuestionData &data)
{
    _question = new Question {data};
    _questionNumber += 1;
    Q_EMIT questionChanged(*_question);
}

void CurrentMatch::setPlayerAnswer(const PlayerAnswerData &data)
{
    _question->setPlayerAnswer(data.playerId, data.playerOption);
    if (data.playerOption == _question->rightOption())
    {
        auto it = std::find_if(_players.begin(), _players.end(), [&](Player *x) {
            return x->id() == data.playerId;
        });
        (*it)->setScore((*it)->score() + 1);
    }
}
