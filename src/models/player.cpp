#include "player.h"

#include "../data/playerdata.h"

#include <QtQml>

Player::Player(const PlayerData &data)
    : QObject(nullptr), _id(data.id), _name(data.playerName), _email(data.playerEmail), _approved(data.approved), _score(0)
{
    qmlRegisterInterface<Player>("Player", 1);
}

int Player::id() const
{
    return _id;
}

QString Player::name() const
{
    return _name;
}

QString Player::email() const
{
    return _email;
}

bool Player::approved() const
{
    return _approved;
}

int Player::score() const
{
    return _score;
}

void Player::setScore(int aux)
{
    _score = aux;
    Q_EMIT scoreChanged(_score);
}
