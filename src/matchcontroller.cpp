#include "matchcontroller.h"

#include "haqtonapi.h"

#include "models.h"
#include "utils.h"

#include <QtQml>
#include <QSslSocket>

MatchController::MatchController(QObject *parent) : QObject(parent), _current(nullptr)
{
    qmlRegisterInterface<MatchController>("MatchController", 1);
    qDebug() << "Device supports OpenSSL: " << QSslSocket::supportsSsl();
}

/////////////////////////////////////////////////////////////////////////////

void MatchController::matchesWaitingForPlayers()
{
    auto reply = HaQtonAPI::MatchesWaitingForPlayers();
    _current = nullptr;
    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            auto list = data.toList();
            _matches = QList<AvailableMatchData> {list.begin(), list.end()};

            Q_EMIT matchesWaitingForPlayersFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT matchesWaitingForPlayersError(serverError(error, bytes));
        }
    );
}

void MatchController::createMatch(const QString &nickname, const QString &email, const QString &description)
{
    auto reply = HaQtonAPI::CreateMatch({
        {"nickname", nickname},
        {"email", email},
        {"description", description},
    });

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            AvailableMatchData match {data};

            if (match.matchPlayers.isEmpty())
            {
                Q_EMIT createMatchError("Empty players list");
                return;
            }

            Q_EMIT setTopic(QString::number(match.topic));

            _current = new CurrentMatch {match};

            Q_EMIT createMatchFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT createMatchError(serverError(error, bytes));
        }
    );
}

void MatchController::startMatch()
{
    if (!_current)
    {
        Q_EMIT startMatchError("No match selected");
        return;
    }

    auto reply = HaQtonAPI::UpdateMatch(_current->matchId(), {
        {"status", 1},
    });

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            Q_EMIT startMatchFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT startMatchError(serverError(error, bytes));
        }
    );
}

void MatchController::finishMatch()
{
    if (!_current)
    {
        Q_EMIT finishMatchError("No match selected");
        return;
    }

    auto reply = HaQtonAPI::UpdateMatch(_current->matchId(), {
        {"status", 2},
    });

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            Q_EMIT finishMatchFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT finishMatchError(serverError(error, bytes));
        }
    );
}

void MatchController::deleteMatch(const int matchId)
{
    auto reply = HaQtonAPI::DeleteMatch(matchId);

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            Q_EMIT deleteMatchFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT deleteMatchError(serverError(error, bytes));
        }
    );
}

void MatchController::enterMatch(const int matchId, const QString &nickname, const QString &email)
{
    auto reply = HaQtonAPI::AddPlayerToMatch(matchId, {
        {"nickname", nickname},
        {"email", email},
    });

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            auto list = data.toList();
            QList<PlayerData> players {list.begin(), list.end()};

            auto it = std::find_if(_matches.begin(), _matches.end(), [=](const AvailableMatchData &x) {
                return x.id == matchId;
            });

            if (it != _matches.end())
            {
                it->matchPlayers = players;

                _current = new CurrentMatch {*it};

                Q_EMIT setTopic(QString::number(it->topic));

                Q_EMIT enterMatchFinished(data);
                return;
            }
            else
            {
                Q_EMIT enterMatchError("No match with this id");
                return;
            }
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT enterMatchError(serverError(error, bytes));
        }
    );
}

void MatchController::approvePlayer(const int playerId)
{
    if (!_current)
    {
        Q_EMIT approvePlayerError("No match selected");
        return;
    }

    auto reply = HaQtonAPI::UpdatePlayer(_current->matchId(), playerId, {
        {"approved", true},
    });

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            Q_EMIT approvePlayerFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT approvePlayerError(serverError(error, bytes));
        }
    );
}

void MatchController::rejectPlayer(const int playerId)
{
    if (!_current)
    {
        Q_EMIT rejectPlayerError("No match selected");
        return;
    }

    auto reply = HaQtonAPI::UpdatePlayer(_current->matchId(), playerId, {
        {"approved", false},
    });

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            Q_EMIT rejectPlayerFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT rejectPlayerError(serverError(error, bytes));
        }
    );
}

void MatchController::changePlayerScore(const int playerId, int score)
{
    if (!_current)
    {
        Q_EMIT changePlayerScoreError("No match selected");
        return;
    }

    auto reply = HaQtonAPI::UpdatePlayer(_current->matchId(), playerId, {
        {"score", score},
    });

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            Q_EMIT changePlayerScoreFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT changePlayerScoreError(serverError(error, bytes));
        }
    );
}

void MatchController::randomQuestion()
{
    if (!_current)
    {
        Q_EMIT randomQuestionError("No match selected");
        return;
    }

    auto reply = HaQtonAPI::RandomQuestion(_current->matchId());

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            Q_EMIT randomQuestionFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT randomQuestionError(serverError(error, bytes));
        }
    );
}

void MatchController::processPlayerAnswer(const int playerId, int questionId, int playerOption)
{
    if (!_current)
    {
        Q_EMIT processPlayerAnswerError("No matchId");
        return;
    }

    auto reply = HaQtonAPI::ProcessPlayerAnswer(_current->matchId(), playerId, {
        {"question_id", questionId},
        {"player_option", playerOption},
    });

    onFinished(reply,
        [&](QByteArray bytes) {
            auto data = jsonVariant(bytes);

            Q_EMIT processPlayerAnswerFinished(data);
        },
        [&](QString error, QByteArray bytes) {
            Q_EMIT processPlayerAnswerError(serverError(error, bytes));
        }
    );
}

//////////////////////////////////////////////////////////////////

void MatchController::processMessage(const QString &message)
{
    auto data = jsonVariantMap(message.toUtf8());

    if (data.contains("message"))
    {
        auto message_type = data["message"].toString();

        if (message_type == "matches_update")
        {
            auto list = data["data"].toList();
            _matches = QList<AvailableMatchData> {list.begin(), list.end()};

            Q_EMIT matchesUpdateMessage(data["data"]);
        }
        else if (message_type == "players_update")
        {
            if (_current)
            {
                auto list = data["data"].toList();
                QList<PlayerData> matchPlayers {list.begin(), list.end()};

                auto it = std::find_if(matchPlayers.begin(), matchPlayers.end(), [&](const PlayerData &x) {
                    return x.id == _current->playerId();
                });

                if (it == matchPlayers.end())
                {
                    Q_EMIT setTopic(QStringLiteral("matches"));
                    Q_EMIT playerBanned();
                    _current = nullptr;
                    return;
                }

                _current->updatePlayers(matchPlayers);

                Q_EMIT playersUpdateMessage(data["data"]);
                return;
            }
        }
        else if (message_type == "new_question")
        {
            if (_current)
            {
                _current->updateQuestion(data["data"]);
                Q_EMIT newQuestionMessage(data["data"]);
                return;
            }
        }
        else if (message_type == "new_answer")
        {
            if (_current)
            {
                _current->setPlayerAnswer(data["data"]);
                Q_EMIT newAnswerMessage(data["data"]);
                return;
            }

        }
        else if (message_type == "match_finished")
        {
            if (_current)
            {
                Q_EMIT setTopic(QStringLiteral("matches"));
                Q_EMIT matchFinishedMessage();
                return;
            }
        }
    }
}

CurrentMatch *MatchController::currentMatch() const
{
    return _current;
}

void MatchController::onFinished(QNetworkReply *reply, ThenFunc then, ErrorFunc error, FinallyFunc finally)
{
    connect(reply, &QNetworkReply::finished, [=]() {
        auto bytes = reply->readAll();
        if (reply->error() == QNetworkReply::NoError)
        {
            then(bytes);
        }
        else
        {
            auto errorStr = reply->errorString();
            qDebug() << errorStr;
            qDebug() << bytes;
            error(errorStr, bytes);
        }
        finally();
        reply->deleteLater();
    });
}

void MatchController::onFinished(QNetworkReply *reply, ThenFunc then, ErrorFunc error)
{
    connect(reply, &QNetworkReply::finished, [=]() {
        auto bytes = reply->readAll();
        if (reply->error() == QNetworkReply::NoError)
        {
            then(bytes);
        }
        else
        {
            auto errorStr = reply->errorString();
            qDebug() << errorStr;
            qDebug() << bytes;
            error(errorStr, bytes);
        }
        reply->deleteLater();
    });
}

void MatchController::onFinished(QNetworkReply *reply, ThenFunc then)
{
    connect(reply, &QNetworkReply::finished, [=]() {
        auto bytes = reply->readAll();
        if (reply->error() == QNetworkReply::NoError)
        {
            then(bytes);
        }
        else
        {
            auto errorStr = reply->errorString();
            qDebug() << errorStr;
            qDebug() << bytes;
        }
        reply->deleteLater();
    });
}
