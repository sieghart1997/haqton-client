#include "haqtonapi.h"

#include <QJsonDocument>

#define HAQTON_API QNetworkReply *
#define HAQTON_RESPONSE(X) X

const auto HaQtonAPI::_AUTHORIZATION = QByteArray {"Bearer eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE2MDU5MzQ1OTksImlhdCI6MTYwMjA0NjU5OSwiaXNzIjoiUW1vYiBTb2x1dGlvbnMiLCJ1c2VyIjp7InVzZXJuYW1lIjoibWF0aGV1cy5nb2lzQGRjb21wLnVmcy5iciJ9fQ.FG1BiiDi2QGaWf5im5ey206qYwLbflUMHR__Df2j5x1Nf4Y-okIpw2lv6FoaM9vUkweLkyeS9iXq9IZbAh-Y_foXPRtKBMsDv_QFWpz4VU0ygq1ZJoKepRVuQPYr5V9yDGmYliWD18lHvFAYjqmTS7mB0ZYs0PHq9cU78Ghxs6HFkgvbe9MTGNViKfB9JlPiACo3-vZPwwWRik-6RIZJaRJ1vHQfhd7i4FAofHp6gKr1IYLn3lDIxm9cd_etB9z_3dPHu2nDi4BjLJP5pLN4Fdg3xpR3exCS4ml8-NbZZOSIFSudMxgo4XiYZPAzfWkLNumox6wTD30Th0RFewTcI2Pabw_L-85ZX9XJxMN8-rb2rEpdLdfEKrRXAwy6n3hdG8a1DRstaXpJqrK6OXMZXHI5rX7UjvkBv9FX9jMdmFidW1xa6oSNzkbAHDABkNAEDbIYZYrx1N1iojaL6X-jkySR8u-xonFe8F8jN0i5Iv_TAPcF0tGQtjcIDRYVTpyZHSpmiH9QZV98IXRzn57b1wkN-3KxxEkVh96ikjUYbjNkVq3i9D0Xdm4wx1umVhE7Z5KCyFV01_2iycym34An_msCj4EtmCreu9Y6zEOoPXBZXnwGAc0JXCBNMNejHeSQAIJHL1a9tbbsJvAN8yyqj237KDLSta2vSNf-aJRhH0s"};
const auto HaQtonAPI::_BASE_URL = QUrl {"https://haqton.qmob.solutions"};

QNetworkAccessManager HaQtonAPI::_manager;

HAQTON_API HaQtonAPI::MatchesWaitingForPlayers()
{
    auto request = _request(QString {"matches"});

    qDebug() << "New request:" << __FUNCTION__ << request.url();

    return HAQTON_RESPONSE(_manager.get(request));
}

HAQTON_API HaQtonAPI::CreateMatch(BODY body)
{
    auto request = _request(QString {"matches"});

    qDebug() << "New request:" << __FUNCTION__ << request.url();

    auto jsonDocument = QJsonDocument::fromVariant(body);

    return HAQTON_RESPONSE(_manager.post(request, jsonDocument.toJson()));
}

HAQTON_API HaQtonAPI::UpdateMatch(ID matchId, BODY body)
{
    auto request = _request(QString {"matches/%1"}.arg(matchId));

    qDebug() << "New request:" << __FUNCTION__ << request.url();

    auto jsonDocument = QJsonDocument::fromVariant(body);

    return HAQTON_RESPONSE(_manager.put(request, jsonDocument.toJson()));
}

HAQTON_API HaQtonAPI::DeleteMatch(ID matchId)
{
    auto request = _request(QString {"matches/%1"}.arg(matchId));

    qDebug() << "New request:" << __FUNCTION__ << request.url();

    return HAQTON_RESPONSE(_manager.deleteResource(request));
}

HAQTON_API HaQtonAPI::AddPlayerToMatch(ID matchId, BODY body)
{
    auto request = _request(QString {"matches/%1/players"}.arg(matchId));

    qDebug() << "New request:" << __FUNCTION__ << request.url();

    auto jsonDocument = QJsonDocument::fromVariant(body);

    return HAQTON_RESPONSE(_manager.post(request, jsonDocument.toJson()));
}

HAQTON_API HaQtonAPI::UpdatePlayer(ID matchId, ID playerId, BODY body)
{
    auto request = _request(QString {"matches/%1/players/%2"}.arg(matchId).arg(playerId));

    qDebug() << "New request:" << __FUNCTION__ << request.url();

    auto jsonDocument = QJsonDocument::fromVariant(body);

    return HAQTON_RESPONSE(_manager.put(request, jsonDocument.toJson()));
}

HAQTON_API HaQtonAPI::RandomQuestion(ID matchId)
{
    auto request = _request(QString {"matches/%1/random_question"}.arg(matchId));

    qDebug() << "New request:" << __FUNCTION__ << request.url();

    return HAQTON_RESPONSE(_manager.get(request));
}

HAQTON_API HaQtonAPI::ProcessPlayerAnswer(ID matchId, ID playerId, BODY body)
{
    auto request = _request(QString {"matches/%1/players/%2/answer"}.arg(matchId).arg(playerId));

    qDebug() << "New request:" << __FUNCTION__ << request.url();

    auto jsonDocument = QJsonDocument::fromVariant(body);

    return HAQTON_RESPONSE(_manager.post(request, jsonDocument.toJson()));
}

QNetworkRequest HaQtonAPI::_request(const QString &relativeUrl)
{
    QNetworkRequest request {_BASE_URL.resolved(relativeUrl)};
    request.setRawHeader("Authorization", _AUTHORIZATION);
    request.setRawHeader("Content-type", "application/json; charset=utf-8");
    request.setRawHeader("Accept", "application/json; charset=utf-8");
    return request;
}
