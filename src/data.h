#ifndef DATAVARIANT_H
#define DATAVARIANT_H

#include "data/answerdata.h"
#include "data/availablematchdata.h"
#include "data/playerdata.h"
#include "data/playeranswerdata.h"
#include "data/questiondata.h"

#endif // DATAVARIANT_H
