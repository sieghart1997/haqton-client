#ifndef MATCHCONTROLLER_H
#define MATCHCONTROLLER_H

#include <QObject>
#include <QPointer>
#include <QNetworkReply>

class CurrentMatch;

#include "data.h"

class MatchController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(CurrentMatch *current READ currentMatch CONSTANT)

public:
    explicit MatchController(QObject *parent = nullptr);

    Q_INVOKABLE void matchesWaitingForPlayers();
    Q_INVOKABLE void createMatch(const QString &nickname, const QString &email, const QString &description);
    Q_INVOKABLE void startMatch();
    Q_INVOKABLE void finishMatch();
    Q_INVOKABLE void deleteMatch(const int matchId);
    Q_INVOKABLE void enterMatch(const int matchId, const QString &nickname, const QString &email);
    Q_INVOKABLE void approvePlayer(const int playerId);
    Q_INVOKABLE void rejectPlayer(const int playerId);
    Q_INVOKABLE void changePlayerScore(const int playerId, int score);
    Q_INVOKABLE void randomQuestion();
    Q_INVOKABLE void processPlayerAnswer(const int playerId, int questionId, int playerOption);

Q_SIGNALS:

#define ROUTE_SIGNAL(ROUTE)\
    void ROUTE ## Finished(const QVariant &);\
    void ROUTE ## Error(const QVariant &);\

    ROUTE_SIGNAL(matchesWaitingForPlayers)
    ROUTE_SIGNAL(createMatch)
    ROUTE_SIGNAL(startMatch)
    ROUTE_SIGNAL(finishMatch)
    ROUTE_SIGNAL(deleteMatch)
    ROUTE_SIGNAL(enterMatch)
    ROUTE_SIGNAL(approvePlayer)
    ROUTE_SIGNAL(rejectPlayer)
    ROUTE_SIGNAL(changePlayerScore)
    ROUTE_SIGNAL(randomQuestion)
    ROUTE_SIGNAL(processPlayerAnswer)

#undef ROUTE_SIGNAL

    void matchesUpdateMessage(const QVariant&);
    void playersUpdateMessage(const QVariant&);
    void newQuestionMessage(const QVariant&);
    void newAnswerMessage(const QVariant&);
    void matchFinishedMessage();
    void playerBanned();

    void setTopic(const QString&);

public:
    void processMessage(const QString &message);

    CurrentMatch *currentMatch() const;

private:
    QList<AvailableMatchData> _matches;
    QPointer<CurrentMatch> _current;

    typedef std::function<void(QByteArray)> ThenFunc;
    typedef std::function<void(QString, QByteArray)> ErrorFunc;
    typedef std::function<void()> FinallyFunc;
    void onFinished(QNetworkReply *reply, ThenFunc then, ErrorFunc error, FinallyFunc finally);
    void onFinished(QNetworkReply *reply, ThenFunc then, ErrorFunc error);
    void onFinished(QNetworkReply *reply, ThenFunc then);
};

#endif // MATCHCONTROLLER_H
