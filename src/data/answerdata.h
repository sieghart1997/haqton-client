#ifndef ANSWERDATA_H
#define ANSWERDATA_H

#include <QObject>

class AnswerData
{
public:
    AnswerData(const QVariant&);

    int id;
    QString description;
};

#endif // ANSWERDATA_H
