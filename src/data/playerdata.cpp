#include "playerdata.h"

#include <QVariant>

PlayerData::PlayerData(const QVariant &data)
{
    auto map = data.toMap();

    id = map["id"].toInt();
    playerName = map["player_name"].toString();
    playerEmail = map["player_email"].toString();
    approved = map["approved"].toBool();
    matchId = map["match_id"].toInt();
}
