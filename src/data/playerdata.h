#ifndef PLAYERDATA_H
#define PLAYERDATA_H

#include <QObject>

class PlayerData
{
public:
    PlayerData(const QVariant&);

    int id;
    QString playerName;
    QString playerEmail;
    bool approved;
    int matchId;
};

#endif // PLAYERDATA_H
