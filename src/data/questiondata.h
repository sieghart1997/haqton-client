#ifndef QUESTIONDATA_H
#define QUESTIONDATA_H

#include <QObject>

class AnswerData;

class QuestionData
{
public:
    QuestionData(const QVariant&);

    int id;
    QString description;
    int rightOption;
    QList<AnswerData> questionOptions;
};

#endif // QUESTIONDATA_H
