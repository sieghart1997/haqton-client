#include "answerdata.h"

#include <QVariant>

AnswerData::AnswerData(const QVariant &data)
{
    auto map = data.toMap();

    id = map["id"].toInt();
    description = map["description"].toString();
}
