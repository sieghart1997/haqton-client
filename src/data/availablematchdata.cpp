#include "availablematchdata.h"

#include "playerdata.h"

#include <QVariant>

AvailableMatchData::AvailableMatchData(const QVariant &data)
{
    auto map = data.toMap();

    id = map["id"].toInt();
    description = map["description"].toString();
    creatorName = map["creator_name"].toString();
    creatorEmail = map["creator_email"].toString();
    topic = map["topic"].toInt();

    auto _match_players = map["match_players"].toList();
    matchPlayers = QList<PlayerData>(_match_players.begin(), _match_players.end());
}
