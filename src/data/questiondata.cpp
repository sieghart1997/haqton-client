#include "questiondata.h"

#include "answerdata.h"

#include <QVariant>

QuestionData::QuestionData(const QVariant &data)
{
    auto map = data.toMap();

    id = map["id"].toInt();
    description = map["description"].toString();
    rightOption = map["right_option"].toInt();

    auto _question_options = map["question_options"].toList();
    questionOptions = QList<AnswerData>(_question_options.begin(), _question_options.end());
}
