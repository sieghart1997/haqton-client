#ifndef AVAILABLEMATCHDATA_H
#define AVAILABLEMATCHDATA_H

#include <QObject>

class PlayerData;

class AvailableMatchData
{
public:
    AvailableMatchData(const QVariant&);

    int id;
    QString description;
    QString creatorName;
    QString creatorEmail;
    int topic;
    QList<PlayerData> matchPlayers;
};

#endif // AVAILABLEMATCHDATA_H
