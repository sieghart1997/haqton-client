#ifndef PLAYERANSWERDATA_H
#define PLAYERANSWERDATA_H

#include <QObject>

class PlayerAnswerData
{
public:
    PlayerAnswerData(const QVariant&);

    int playerId;
    int playerOption;
};

#endif // PLAYERANSWERDATA_H
