#include "playeranswerdata.h"

#include <QVariant>

PlayerAnswerData::PlayerAnswerData(const QVariant &data)
{
    auto map = data.toMap();

    playerId = map["player_id"].toInt();
    playerOption = map["player_option"].toInt();
}
