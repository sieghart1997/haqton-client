#ifndef PLAYERINFO_H
#define PLAYERINFO_H

#include <QObject>

class PlayerInfo : public QObject
{
    Q_OBJECT
	Q_PROPERTY(QString nickname READ nickname WRITE setNickname NOTIFY nicknameChanged)
	Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
	
public:
    explicit PlayerInfo(QObject *parent = nullptr);

	QString nickname();
    void setNickname(const QString &nickname);
	
	QString email();
    void setEmail(const QString &email);

signals:
    void nicknameChanged();
	void emailChanged();

private:
    QString _nickname;
	QString _email;

};

#endif // PLAYERINFO_H
