#ifndef UTILS_H
#define UTILS_H

#include <QJsonDocument>
#include <QVariant>

inline QVariant jsonVariant(const QByteArray &bytes)
{
    return QJsonDocument::fromJson(bytes).toVariant();
}

inline QVariantMap jsonVariantMap(const QByteArray &bytes)
{
    return jsonVariant(bytes).toMap();
}

inline QVariant serverError(const QString &error, const QByteArray &bytes)
{
    auto data = jsonVariant(bytes);
    if (data.isNull())
    {
        return error;
    }
    return data.toMap()["error"];
}

#endif // UTILS_H
