import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15
import QtGraphicalEffects 1.15


Item {
    property var question
    property int number: 1
    property int indexSelected: -1
    property bool buttonsEnabled: true

    property int pauseAnimationTransition: 300

    signal optionSelected(var index)

    implicitWidth: columnLayout.width
    implicitHeight: columnLayout.height

    ColumnLayout {
        id: columnLayout
        width: parent.width

        Item {
            Layout.fillWidth: true
            Layout.minimumHeight: questionLayout.height > 150 ? questionLayout.height : 150
            Layout.maximumHeight: questionLayout.height > 150 ? questionLayout.height : 150
            Layout.margins: 16
            // Layout.bottomMargin: 0

            Rectangle {
                anchors.fill: parent
                radius: 10
                color: "light green"

                SequentialAnimation on rotation {
                    PauseAnimation { duration: pauseAnimationTransition; }
                    RotationAnimator {
                        from: 0;
                        to: 4;
                        duration: 1000
                        easing {type: Easing.OutBack; overshoot: 4}
                    }
                }
            }

            Rectangle {
                anchors.fill: parent
                radius: 10
                color: "light blue"

                SequentialAnimation on rotation {
                    PauseAnimation { duration: pauseAnimationTransition; }
                    RotationAnimator {
                        from: 0;
                        to: -4;
                        duration: 1000
                        easing {type: Easing.OutBack; overshoot: 4}
                    }
                }
            }

            Rectangle {
                id: questionRect
                anchors.fill: parent
                radius: 10
                color: "white"
                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 0
                    verticalOffset: 3
                    radius: 8.0
                    samples: 15
                    color: "#80000000"
                }

                Item {
                    id: questionItem
                    anchors.fill: parent
                    anchors.margins: 16
                    Column {
                        id: questionLayout

                        Label {
                            text: "Questão " + number
                        }
                        Label {
                            width: questionItem.width
                            text: question.description
                            wrapMode: Label.WordWrap
                        }
                    }
                }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.minimumHeight: answersLayout.height
            Layout.maximumHeight: answersLayout.height
            Layout.margins: 16
            Layout.topMargin: 0

            ColumnLayout {
                id: answersLayout
                width: parent.width

                Repeater {
                    model: question.questionOptions.length
                    delegate: Rectangle {
                        Layout.fillWidth: true
                        Layout.minimumHeight: 35
                        Layout.margins: 5
                        border.width: 1
                        border.color: "grey"
                        radius: 10
                        opacity: 0
                        color: indexSelected === index ? "light green" : "white"

                        SequentialAnimation on opacity {
                            PauseAnimation { duration: index * 100 + pauseAnimationTransition; }
                            NumberAnimation { from: 0; to: 1; duration: 1000; easing.type: Easing.InOutQuad; }
                        }

                        Label {
                            anchors.left: parent.left
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.leftMargin: 10
                            text: "" + (index + 1)
                        }

                        Label {
                            anchors.centerIn: parent
                            text: question.questionOptions[index]
                        }

                        MouseArea {
                            enabled: buttonsEnabled
                            anchors.fill: parent
                            onClicked: {
                                optionSelected(index);
                            }
                        }
                    }
                }
            }
        }

    }
}
