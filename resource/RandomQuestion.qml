import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15
import QtGraphicalEffects 1.15

import solutions.qmob.haqton 1.0

Item {
    id: root

    property int answerIndexSelected: -1
    property int nAnswers: 0

    signal pushStack(var item, var properties)
    signal replaceStack(var item, var properties)
    signal popStack(var item)
    signal dialog(var title, var message)

    Connections {
        target: Core.match
        function onProcessPlayerAnswerFinished(data) {
            answerIndexSelected = data.player_option;
        }
        function onProcessPlayerAnswerError(error) {
            dialog("Erro", error);
        }
        function onNewAnswerMessage(data) {
            nAnswers += 1;

            if (nAnswers === Core.match.current.players.length) {
                replaceStack("MatchAnswers.qml", {});
            }
        }
    }

    ColumnLayout {
        width: parent.width

        Question {
            question: Core.match.current.question
            number: Core.match.current.questionNumber
            indexSelected: root.answerIndexSelected
            buttonsEnabled: answerIndexSelected === -1

            Layout.fillWidth: true
            Layout.topMargin: 100

            Connections {
                function onOptionSelected(index) {
                    if (answerIndexSelected === -1) {
                        Core.match.processPlayerAnswer(Core.match.current.playerId, Core.match.current.question.id, index);
                    }
                }
            }
        }

        PlayerStatus {
            score: Core.match.current.me.score
            answered: answerIndexSelected !== -1
            Layout.fillWidth: true
            Layout.topMargin: 50
        }
    }

}
