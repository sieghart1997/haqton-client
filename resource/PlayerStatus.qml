import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15
import QtGraphicalEffects 1.15

Item {
    property int score: 0
    property bool answered: false

    id: playerStatusItem
    opacity: 0
    SequentialAnimation on opacity {
        PauseAnimation { duration: 500; }
        NumberAnimation { from: 0; to: 1; duration: 500; easing.type: Easing.InOutQuad; }
    }

    ColumnLayout {
        anchors.centerIn: playerStatusItem

        Label {
            Layout.alignment: Qt.AlignHCenter
            text: answered ? "Respondido" : "Aguardando responder"
            color: "white"
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            Rectangle {
                width: 30
                height: 1
                color: "white"
                opacity: 0.2
            }

            Label {
                text: "SCORE: " + score
                color: "white"
            }

            Rectangle {
                width: 30
                height: 1
                color: "white"
                opacity: 0.2
            }
        }
    }

}
