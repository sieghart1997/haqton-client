import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

Item {
    signal pushStack(var item, var properties)
    signal popStack()
    signal dialog(var title, var message)

    property var matches: []

    FontLoader { id: font; source: "assets/fonts/PocketMonk-15ze.ttf" }

    Component.onCompleted: {
        Core.match.matchesWaitingForPlayers();
    }

    Connections {
        target: Core.match
        function onMatchesWaitingForPlayersFinished(data) {
            matches = data;
            listView.selectedIndex = -1;
        }
        function onMatchesWaitingForPlayersError(error) {
            dialog("Erro", error);
            listView.selectedIndex = -1;
        }
        function onMatchesUpdateMessage(data) {
            matches = data;
            listView.selectedIndex = -1;
        }
        function onPlayerBanned() {
            Core.match.matchesWaitingForPlayers();
        }
    }

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft

            BackButton {
                onClicked: {
                    popStack();
                }
            }


            Label {
                id: enterMatchLabel
                font { pixelSize: 25 }
                color: "white"; text: "Participar de uma partida"
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
        }

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            clip: true

            ScrollBar.horizontal.interactive: false
            ScrollBar.vertical.interactive: true
            ScrollBar.vertical.policy: ScrollBar.AsNeeded

            ListView {
                property int selectedIndex: -1
                property var selectedItem: null

                id: listView
                spacing: 10
                model: matches.length

                delegate: Item {
                    id: item
                    width: listView.width
                    height: rect.height

                    function isSelected() {
                        return listView.selectedIndex == index;
                    }

                    Rectangle {
                        id: rect
                        color: isSelected() ? "#34C5F4" : "white"
                        radius: 10
                        width: parent.width - 20
                        height: childrenRect.height
                        anchors.horizontalCenter: parent.horizontalCenter

                        Column {
                            padding: 10
                            Label {
                                text: "Dono da partida: " + matches[index].creator_name
                                color: isSelected() ? "white" : "#3A3949"
                            }
                            Label {
                                text: "Email do dono: " + matches[index].creator_email
                                color: isSelected() ? "white" : "#3A3949"
                            }
                            Label {
                                text: "Descricao: " + matches[index].description
                                color: isSelected() ? "white" : "#3A3949"
                            }
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if (listView.selectedIndex == index) {
                                    listView.selectedIndex = -1;
                                    listView.selectedItem = null;
                                    return;
                                }
                                listView.selectedIndex = index;
                                listView.selectedItem = matches[index];
                            }
                        }

                    }
                }

            }
        }

        RoundButton {
            property bool loading: false

            id: confirmButton
            visible: listView.selectedIndex != -1
            Material.background: "#00A000"
            Layout.fillWidth: true
            height: 40
            radius: 10
            enabled: !loading

            contentItem: Label {
                text: confirmButton.loading ? "Carregando..." : "Participar"
                font: font.name
                horizontalAlignment: Text.AlignHCenter
                color: "white"
                wrapMode: Text.WordWrap
            }

            onClicked: {
                const nickname = Core.playerInfo.nickname;
                const email = Core.playerInfo.email;
                const match = listView.selectedItem;
                confirmButton.loading = true;
                Core.match.enterMatch(match.id, nickname, email);
            }

            Connections {
                target: Core.match
                function onEnterMatchFinished(data) {
                    confirmButton.loading = false;
                    listView.selectedIndex = -1;
                    pushStack("WaitingPlayers.qml", {});
                }
                function onEnterMatchError(error) {
                    confirmButton.loading = false;
                    listView.selectedIndex = -1;
                    dialog("Erro", error);
                }
            }
        }

    }

}
