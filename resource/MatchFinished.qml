import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

Item {
    property int pauseAnimationTransition: 300

    signal pushStack(var item, var properties)
    signal replaceStack(var item, var properties)
    signal popStack(var item)

    FontLoader { id: font; source: "assets/fonts/PocketMonk-15ze.ttf" }

    ColumnLayout {
        width: parent.width
        height: parent.height - bottomBar.height

        Label {
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: 50
            Layout.bottomMargin: 10
            font { pixelSize: 25 }
            text: "Partida finalizada"
            color: "white"
        }

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.topMargin: 10

            clip: true

            ScrollBar.horizontal.interactive: false
            ScrollBar.vertical.interactive: true
            ScrollBar.vertical.policy: ScrollBar.AsNeeded

            ListView {
                id: listView
                anchors.fill: parent
                spacing: 10
                model: Core.match.current.sortedPlayers.length

                anchors.leftMargin: 10
                anchors.rightMargin: 10
                anchors.bottomMargin: 10

                delegate: Rectangle {
                    width: listView.width
                    height: labelsLayout.height

                    border.width: 1
                    border.color: "grey"
                    radius: 10
                    opacity: 0
                    color: "white"

                    SequentialAnimation on opacity {
                        PauseAnimation { duration: index * 100 + pauseAnimationTransition; }
                        NumberAnimation { from: 0; to: 1; duration: 500; easing.type: Easing.InOutQuad; }
                    }

                    RowLayout {
                        id: playersLayout
                        width: parent.width

                        ColumnLayout {
                            id: labelsLayout
                            Layout.alignment: Qt.AlignLeft
                            Layout.fillWidth: true

                            Label {
                                Layout.margins: 5
                                text: Core.match.current.sortedPlayers[index].name
                            }
                            Label {
                                Layout.margins: 5
                                text: Core.match.current.sortedPlayers[index].email
                            }

                        }

                        RowLayout {
                            Layout.alignment: Qt.AlignRight
                            Layout.rightMargin: 10

                            Label {
                                Layout.alignment: Qt.AlignLeft
                                text: "SCORE:"
                            }

                            Rectangle {
                                Layout.alignment: Qt.AlignRight
                                Layout.minimumWidth: playersLayout.height / 2
                                Layout.minimumHeight: playersLayout.height / 2
                                radius: playersLayout.height
                                border.color: "grey"
                                border.width: 1

                                Label {
                                    anchors.centerIn: parent

                                    text: "" + Core.match.current.sortedPlayers[index].score
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    RoundButton {
        id: bottomBar
        width: parent.width
        Material.background: "#00A000"
        anchors.bottom: parent.bottom
        height: 50
        radius: 10

        contentItem: Label {
            text: "Retornar"
            font: font.name
            horizontalAlignment: Text.AlignHCenter
            color: "white"
            wrapMode: Text.WordWrap
        }

        onClicked: {
            popStack(null);
        }
    }

}
