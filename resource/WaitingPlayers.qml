import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

Item {
    signal pushStack(var item, var properties)
    signal popStack()
    signal dialog(var title, var message)

    function allPlayersApproved() {
        return Core.match.current.players.every((element, index, array) => {
            return element.approved;
        });
    }

    Connections {
        target: Core.match
        function onNewQuestionMessage(data) {
            pushStack("RandomQuestion.qml", {});
        }
        function onPlayerBanned() {
            dialog("Voce foi banido da partida", null);
            popStack();
        }
    }

    FontLoader { id: font; source: "assets/fonts/PocketMonk-15ze.ttf" }

    ColumnLayout {
        anchors.fill: parent

        Label {
            id: enterMatchLabel
            font { pixelSize: 18 }
            color: "white"; text: "Aguardando chagada dos jogadores"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.topMargin: 50
            wrapMode: Text.WordWrap
        }

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            clip: true

            ScrollBar.horizontal.interactive: false
            ScrollBar.vertical.interactive: true
            ScrollBar.vertical.policy: ScrollBar.AsNeeded

            ListView {
                id: listView
                spacing: 10
                model: Core.match.current.players.length

                delegate: Item {
                    id: item
                    width: listView.width
                    height: rect.height

                    function approved() { return Core.match.current.players[index].approved }

                    Rectangle {
                        id: rect
                        color: approved() ? "#34C5F4" : "white"
                        radius: 10
                        width: parent.width - 20
                        height: childrenRect.height
                        anchors.horizontalCenter: parent.horizontalCenter

                        RowLayout {
                            Column {
                                padding: 10
                                Label {
                                    text: "Nome: " + Core.match.current.players[index].name + (Core.match.current.playerId === Core.match.current.players[index].id ? " --> Voce <--" : "")
                                    color: approved() ? "white" : "#3A3949"
                                }
                                Label {
                                    text: "Email: " + Core.match.current.players[index].email
                                    color: approved() ? "white" : "#3A3949"
                                }
                                Label {
                                    text: approved() ? "Aprovado" : "Aguardando"
                                    color: approved() ? "white" : "#3A3949"
                                }
                            }
							
							Button {
								id: button1
                                visible: Core.match.current.isOwner && !approved()

								text: "Aprovar"

								contentItem: Label {
									id: textLabel1
									text: button1.text
									font: button1.font
									horizontalAlignment: Text.AlignHCenter
									color: "#1b1b1b"
									wrapMode: Text.WordWrap
								}
							
								topInset: 0; bottomInset: 0
								font.capitalization: Font.AllUppercase
							
								Material.background: "white"
								onClicked: {
                                    const player = Core.match.current.players[index];
                                    Core.match.approvePlayer(player.id);
                                }
							}
							
							Button {
								id: button2
                                visible: Core.match.current.isOwner && !approved()
								
								text: "Banir"

								contentItem: Label {
									id: textLabel2
									text: button2.text
									font: button2.font
									horizontalAlignment: Text.AlignHCenter
									color: "#1b1b1b"
									wrapMode: Text.WordWrap
								}
							
								topInset: 0; bottomInset: 0
								font.capitalization: Font.AllUppercase
							
								Material.background: "red"
								onClicked: {
                                    const player = Core.match.current.players[index];
                                    Core.match.rejectPlayer(player.id);
                                }
							}

                        }

                    }
                }
            }
        }

        RoundButton {
            property bool loading: false

            id: confirmButton
            visible: Core.match.current.isOwner && allPlayersApproved()
            Material.background: "#00A000"
            Layout.fillWidth: true
            height: 40
            radius: 10
            enabled: !loading

            contentItem: Label {
                text: confirmButton.loading ? "Carregando..." : "Comecar"
                font: font.name
                horizontalAlignment: Text.AlignHCenter
                color: "white"
                wrapMode: Text.WordWrap
            }

            onClicked: {
                confirmButton.loading = true;
                Core.match.startMatch();
            }

            Connections {
                target: Core.match
                function onStartMatchFinished(data) {
                    confirmButton.loading = true;
                    Core.match.randomQuestion();
                }
                function onStartMatchError(error) {
                    dialog("Erro", error);
                    confirmButton.loading = false;
                }
            }
        }

    }
}
