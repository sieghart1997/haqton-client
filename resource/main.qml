/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

ApplicationWindow {
    width: 380
    height: width * 16/9
    visible: true
    title: qsTr("HaQton")

//    Audio { source: "assets/audio/Magic-Clock-Shop_Looping.mp3"; loops: Audio.Infinite; autoPlay: true }

    Rectangle {
        id: defaultBackground
        anchors.fill: parent
        color: "#3A3949"
        // #874FCC
        // #EE6844

        StackNavigator {
            id: stackNavigator
            anchors.fill: parent
            initialItem: "PlayerInfo.qml"
        }

    }

    MainDialog {
        anchors.centerIn: parent
        target: stackNavigator.currentItem
    }

}
