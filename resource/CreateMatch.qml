import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

Item {
    signal pushStack(var item, var properties)
    signal popStack()
    signal dialog(var title, var message)

    FontLoader { id: font; source: "assets/fonts/PocketMonk-15ze.ttf" }

    RowLayout {

        BackButton {
            onClicked: {
                popStack();
            }
        }
    }

    Label {
        id: descriptionLabel
        font { pixelSize: 32 }
        color: "white"; text: "Descrição da partida"
        anchors.horizontalCenter: parent.horizontalCenter

        NumberAnimation on opacity {
            from: 0
            to: 1
            duration: 1000
            easing.type: Easing.InQuad
        }

        NumberAnimation on y {
            from: 70
            to: 50
            duration: 1000
            easing.type: Easing.InQuad
        }
    }

    TextArea {
        id: descriptionTextId
        placeholderText: qsTr("Digite aqui")

        anchors.centerIn: parent

        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        wrapMode: Text.WordWrap

        background: Rectangle {
            implicitWidth: 250
            radius: 10
            color: "white"
            border.color: "green"
        }

        NumberAnimation on opacity {
            from: 0
            to: 1
            duration: 1000
            easing.type: Easing.InQuad
        }
    }

    RoundButton {
        property bool loading: false

        id: confirmButton
        Material.background: "#00A000"

        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 20
        width: 250
        height: 50
        radius: 10
        enabled: !loading

        contentItem: Label {
            text: confirmButton.loading ? "Carregando..." : "Confirmar"
            font: font.name
            horizontalAlignment: Text.AlignHCenter
            color: "white"
            wrapMode: Text.WordWrap
        }

        onClicked: {
            const nickname = Core.playerInfo.nickname;
            const email = Core.playerInfo.email;
            confirmButton.loading = true;
            Core.match.createMatch(nickname, email, descriptionTextId.text);
        }

        Connections {
            target: Core.match
            function onCreateMatchFinished(data) {
                pushStack("WaitingPlayers.qml", {});
                confirmButton.loading = false;
            }
            function onCreateMatchError(error) {
                confirmButton.loading = false;
                dialog("Erro", error);
            }
        }
    }

}
