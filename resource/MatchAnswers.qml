import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

Item {
    property int pauseAnimationTransition: 300

    signal pushStack(var item, var properties)
    signal replaceStack(var item, var properties)
    signal popStack(var item)
    signal dialog(var title, var message)

    ColumnLayout {
        width: parent.width
        height: parent.height - bottomBar.height

        Label {
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: 50
            font { pixelSize: Core.match.current.isOwner ? 30 : 20 }
            text: Core.match.current.isOwner ? "Respostas" : "Esperando a proxima pergunta"
            color: "white"
        }

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.topMargin: 10

            clip: true

            ScrollBar.horizontal.interactive: false
            ScrollBar.vertical.interactive: true
            ScrollBar.vertical.policy: ScrollBar.AsNeeded

            ListView {
                id: listView
                anchors.fill: parent
                spacing: 10
                model: Core.match.current.players.length

                anchors.leftMargin: 10
                anchors.rightMargin: 10
                anchors.bottomMargin: 10

                delegate: Rectangle {
                    width: listView.width
                    height: labelsLayout.height

                    border.width: 1
                    border.color: "grey"
                    radius: 10
                    opacity: 0
                    color: "white"

                    SequentialAnimation on opacity {
                        PauseAnimation { duration: index * 100 + pauseAnimationTransition; }
                        NumberAnimation { from: 0; to: 1; duration: 500; easing.type: Easing.InOutQuad; }
                    }

                    RowLayout {
                        id: playersLayout
                        width: parent.width

                        ColumnLayout {
                            id: labelsLayout
                            Layout.alignment: Qt.AlignLeft
                            Layout.fillWidth: true

                            Label {
                                Layout.margins: 5
                                text: Core.match.current.players[index].name
                            }
                            Label {
                                Layout.margins: 5
                                text: Core.match.current.players[index].email
                            }

                        }

                        RowLayout {
                            Layout.alignment: Qt.AlignRight
                            Layout.rightMargin: 10

                            Label {
                                Layout.alignment: Qt.AlignLeft
                                text: "Resposta:"
                            }

                            Rectangle {
                                Layout.alignment: Qt.AlignRight
                                Layout.minimumWidth: playersLayout.height / 2
                                Layout.minimumHeight: playersLayout.height / 2
                                radius: playersLayout.height
                                border.color: "grey"
                                border.width: 1

                                Label {
                                    anchors.centerIn: parent
                                    function playerAnswer() {
                                        const playerId = Core.match.current.players[index].id;
                                        const answer = Core.match.current.question.playerAnswers[playerId.toString()];
                                        return answer + 1;
                                    }
                                    text: playerAnswer()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Item {
        property bool loading: false
        id: bottomBar
        visible: Core.match.current.isOwner
        width: parent.width
        height: 50
        anchors.bottom: parent.bottom

        RowLayout {
            id: buttonsLayout
            anchors.fill: parent
            spacing: 5
            anchors.leftMargin: 5
            anchors.rightMargin: 5

            Button {
                enabled: !bottomBar.loading
                Layout.fillWidth: true
                Layout.minimumHeight: 50
                Layout.maximumHeight: 50
                Material.background: "light green"
                contentItem: Label {
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Encerrar partida"
                }
                onClicked: {
                    bottomBar.loading = true;
                    Core.match.finishMatch();
                }
            }
            Button {
                enabled: !bottomBar.loading
                Layout.fillWidth: true
                Layout.minimumHeight: 50
                Layout.maximumHeight: 50
                Material.background: "light blue"
                contentItem: Label {
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Próxima pergunta"
                }
                onClicked: {
                    bottomBar.loading = true;
                    Core.match.randomQuestion();
                }
            }
        }

        Connections {
            target: Core.match
            function onFinishMatchFinished(data) {
                bottomBar.loading = false;
            }
            function onFinishMatchError(error) {
                bottomBar.loading = false;
                dialog("Erro", error);
            }
            function onRandomQuestionFinished(data) {
                bottomBar.loading = false;
            }
            function onRandomQuestionError(error) {
                bottomBar.loading = false;
//                dialog("Erro", error);
            }
            function onMatchFinishedMessage() {
                pushStack("MatchFinished.qml", {});
                bottomBar.loading = false;
            }
            function onNewQuestionMessage(data) {
                replaceStack("RandomQuestion.qml", {});
                bottomBar.loading = false;
            }
        }
    }

}
