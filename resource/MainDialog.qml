import QtQuick 2.15
import QtQuick.Controls 2.15

Dialog {
    id: dialog

    property alias target: conn.target

    modal: true

    Label {
        id: label
        visible: Boolean(label.text)
    }

    Connections {
        id: conn
        ignoreUnknownSignals: true
        function onDialog(title, message) {
            dialog.title = title;
            if (message !== undefined) {
                label.text = message;
            }
            dialog.open();
        }
    }
}
