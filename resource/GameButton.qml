import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "FontAwesome"

Button {
    id: button

    property alias iconLabel: iconLabel
    property int textSize: 64
    property var backgroundColor: "white"

    contentItem: Item {
        implicitHeight: iconLabel.implicitHeight + textLabel.implicitHeight
        ColumnLayout {
            anchors.centerIn: parent
            width: parent.width; height: parent.height
            Label {
                id: iconLabel
                Layout.alignment: Qt.AlignHCenter
                font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: textSize }
            }
            Label {
                id: textLabel
                text: button.text
                font: button.font
                Layout.fillWidth: true; Layout.fillHeight: true
                horizontalAlignment: Text.AlignHCenter
                color: "#1b1b1b"
                wrapMode: Text.WordWrap
            }
        }
    }

    topInset: 0; bottomInset: 0
    font.capitalization: Font.AllUppercase

    Material.background: backgroundColor
}
