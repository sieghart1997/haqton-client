import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

import "FontAwesome"

import solutions.qmob.haqton 1.0

Item {
    readonly property int textWidth: 250
    readonly property int textHeight: 40
    readonly property int textBorderRadius: 10

    signal replaceStack(var item, var properties)

    FontLoader { id: font; source: "assets/fonts/PocketMonk-15ze.ttf" }

    ColumnLayout {
        anchors.centerIn: parent

        HaQtonLabel {
            Layout.alignment: Qt.AlignHCenter
            Layout.margins: 10
        }

        TextField {
            id: nicknameTextId
            placeholderText: qsTr("nickname")
            text: "test"

            Layout.alignment: Qt.AlignHCenter
            Layout.margins: 10

            anchors.leftMargin: 10
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter

            background: Rectangle {
                implicitWidth: textWidth
                implicitHeight: textHeight
                radius: textBorderRadius
                color: "white"
                border.color: "green"
            }

            validator: RegExpValidator { regExp: /[^0-9]\w+/ }
        }
        TextField {
            id: emailTextId
            placeholderText: qsTr("email")
            text: "test@gmail.com"

            Layout.alignment: Qt.AlignHCenter
            Layout.margins: 10

            anchors.leftMargin: 10
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter

            background: Rectangle {
                implicitWidth: textWidth
                implicitHeight: textHeight
                radius: textBorderRadius
                color: "white"
                border.color: "green"
            }

            validator: RegExpValidator { regExp: /[A-Za-z0-9.]+@[A-Za-z0-9]+\.[A-Za-z]+\.([A-Za-z]+)?/ }
        }
        Button {
            id: confirmButton
            Layout.alignment: Qt.AlignHCenter
            Layout.margins: 10
            Material.background: "white"

            Layout.fillWidth: true
            Layout.minimumHeight: 40

            contentItem: Label {
                text: "Confirmar"
                font: font.name
                horizontalAlignment: Text.AlignHCenter
                color: "#1b1b1b"
                wrapMode: Text.WordWrap
            }

            onClicked: {
                if (nicknameTextId.text === "") {
                    return;
                }
                if (emailTextId.text === "") {
                    return;
                }

                Core.playerInfo.nickname = nicknameTextId.text;
                Core.playerInfo.email = emailTextId.text;

                replaceStack("InitialMenu.qml", {});
            }
        }
    }
}
