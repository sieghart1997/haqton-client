import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

Label {
    FontLoader { id: font; source: "assets/fonts/PocketMonk-15ze.ttf" }

    font { family: font.name; pixelSize: 64 }
    color: "white"; text: "HaQton!"
}
