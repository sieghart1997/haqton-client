import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "FontAwesome"

Button {
    id: backButton

    property double size: 40

    contentItem: Label {
        text: Icons.faArrowCircleLeft
        font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: size }
        horizontalAlignment: Text.AlignHCenter
        color: "white"
        wrapMode: Text.WordWrap
    }

    background: Rectangle {
        opacity: 0
        border.color: "white"
        border.width: 1
        radius: size
    }

    topInset: 0; bottomInset: 0
    font.capitalization: Font.AllUppercase
}
