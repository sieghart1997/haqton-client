import QtQuick 2.15
import QtQuick.Controls 2.15

StackView {
    id: stackView

    Connections {
        target: stackView.currentItem
        ignoreUnknownSignals: true
        function onPushStack(item, properties, operation) {
            stackView.push(item, properties, operation);
        }
        function onReplaceStack(target, item, properties, operation) {
            stackView.replace(target, item, properties, operation);
        }
        function onPopStack(item, operation) {
            stackView.pop(item, operation);
        }
    }
}
