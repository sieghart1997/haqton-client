import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "FontAwesome"

import solutions.qmob.haqton 1.0

Item {
    signal pushStack(var item, var properties)
    signal dialog(var title, var message)

    FontLoader { id: font; source: "assets/fonts/PocketMonk-15ze.ttf" }

    RowLayout {
        Label {
            font { pixelSize: 12 }
            color: "#3A3949"
            text: "nickname: " + Core.playerInfo.nickname
            Layout.margins: 10
        }
        Label {
            font { pixelSize: 12 }
            color: "#3A3949"
            text: "email: " + Core.playerInfo.email
            Layout.margins: 10
        }
    }

    ColumnLayout {
        width: parent.width
        anchors { verticalCenter: parent.verticalCenter; }

        HaQtonLabel {
            Layout.alignment: Qt.AlignHCenter
            Layout.margins: 10
        }

        GridLayout {
            columns: 2; columnSpacing: 5; rowSpacing: 5
            Layout.fillWidth: true; Layout.fillHeight: false
            Layout.margins: 10
            Repeater {
                id: buttons
                property var functions: [
                    function () {
                        pushStack("CreateMatch.qml", {});
                    },
                    function () {
                        pushStack("EnterMatch.qml", {});
                    },
                    function () {

                    },
                    function () {
                    },
                ]
                model: [
                    {
                        icon: Icons.faPlusCircle,
                        text: "Criar uma partida",
                    },
                    {
                        icon: Icons.faGamepad,
                        text: "Participar de uma partida",
                    },
                    {
                        icon: Icons.faDiceD20,
                        text: "Sobre o HaQton",
                    },
                    {
                        icon: Icons.faMedal,
                        text: "Patrocinadores",
                    },
                ]
                GameButton {
                    Layout.fillWidth: true; Layout.fillHeight: true
                    iconLabel { text: modelData.icon; color: "#3A3949" }
                    text: modelData.text
                    onClicked: {
                        buttons.functions[index]();
                    }
                }
            }
        }
    }
}

